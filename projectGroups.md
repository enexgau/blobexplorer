# Introduction

Project groups are stored as subdirectories under the container 'site-images' as part of the storage account for photos. In order to get data for a specific project, you must first find the relevant subdirectory. There are two main ways which this can be done:

# Method 1

1. Go to Django admin and navigate to photos
2. On the side there is 'filter by ad cache'
3. Select the ad cache you want
4. Under the list of photos which appear the first two words separated by slashes are the subdirectory e.g. user_uploads/60 for project EWR. So the command would be details site-images/user_uploads/60

# Method 2

1. Open a photo which is part of the project via lefindr
2. The part of the url after ...windows.net/**this text here**/jfhasougasoge also represents the subdirectory
