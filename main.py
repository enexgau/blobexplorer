import os, uuid, sys
import numpy as np
import pandas as pd
from datetime import datetime
from pytz import timezone
from environs import Env
from functools import partial
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

# A dictionary with keys = container name and values = another dictionary
# which stores the date/hour as the key and the freq as the value
dateFreq = {}
hourFreq = {}

# Function which quits the program
def quit():
    sys.exit()

# Function which lists out all commands which can be used
def help():
    print("{:15} -- lists all containers in the current storage account".format("containers"))
    print("{:15} -- lists all blobs in the current storage account (may take a while)".format("allblobs"))
    print("{:15} -- Provides detailed statistical summaries on all blobs".format("alldetails"))
    print("{:15} -- Exports all upload times for different projects into /data (may take a while)".format("alltimefreq"))
    print("{:15} -- lists all blobs in the the container <name>".format("blobs <name>"))
    print("{:15} -- The upload times for blobs in container <name>".format("timefreq <name>"))
    print("{:15} -- Detailed statistical summary for container <name>".format("details <name>"))
    print("{:15} -- Prints this!".format("help"))
    print("{:15} -- Quits the program".format("quit"))

# Function which lists all the containers in the storage account
# param: containersList - list of containers
def listContainers(containersList):
    counter = 0
    for nextContainer in containersList:
        print(nextContainer['name'])
        counter += 1

    print(f"There are {counter} containers within this storage account.")

# Function which lists ALL the blobs in the storage account
# param: client - The BlobServiceClient associated with the storage account
# param: containersList - The list of containers associated with the storage account
# param: dateFreq - The 2 dimensional dictionary associated with Date frequency data
# param: hourFreq - The 2 dimensional dictionary associated with Hour frequency data
def listAllBlobs(client, containersList, dateFreq, hourFreq):
    containerCounter = 0
    blobCounter = 0

    for nextContainer in containersList:
        currentContainer = client.get_container_client(nextContainer['name'])
        containerCounter += 1

        print("\n" + nextContainer['name'] + ":")

        # Listing all the blobs is the same as listing all the blobs for each container
        blobCounter += listContainerBlobs(currentContainer, dateFreq, hourFreq, False)

    print(f"\nIn {containerCounter} Containers, there are a total of {blobCounter} blobs")

# Function which lists all the blobs for a specified container
# param: container - The container client associated with the container we're interested in
# param: dateFreq - The 2 dimensional dictionary associated with Date frequency data
# param: hourFreq - The 2 dimensional dictionary associated with Hour frequency data
# param: printSummary - Optional argument on whether we want a summary printed or not
# return: counter - The number of blobs in the container
def listContainerBlobs(container, dateFreq, hourFreq, printSummary=True, path=None):
    counter = 0
    hasTimeData = True # boolean which represents whether we have the date/hour data for this container already

    if(dateFreq.get(container.container_name) == None and path == None):
        #we don't have the date/hour data
        hasTimeData = False

        #Adding an empty dictionary as the value with a key = the container name
        dateFreq[container.container_name] = {}
        hourFreq[container.container_name] = {}

    for nextBlob in container.list_blobs(name_starts_with=path):
        if not hasTimeData:
            # We call parse time to add the time data for each blob
            parseTime(nextBlob, container, dateFreq, hourFreq)

        print(nextBlob.name)
        counter += 1

    if printSummary:
        if path == None:
            print(f"In {container.container_name}, there are {counter} blobs")
        else:
            print(f"In {container.container_name}/{path}, there are {counter} blobs")

    return counter

# Function which provides a statistical summary on the amount of blobs,
# total size, avg size and metadata of blobs in a specified container
# param: container - The container client associated with the container we're interested in
# return dataTypes - dictionary of datatypes and frequency along with the sizeCount,
#                    blobsCount and metadataCount appended at the end
def containerStats(container, path=None):
    sizeCount = 0
    blobsCount = 0
    metadataCount = 0
    dataTypes = {}
    hasTimeData = True

    if(dateFreq.get(container.container_name) == None and path == None):
        # we don't have the date/hour data, we only perform this when detailing
        # the container as a whole, not certain paths
        hasTimeData = False

        #Adding an empty dictionary as the value with a key = the container name
        dateFreq[container.container_name] = {}
        hourFreq[container.container_name] = {}

    for nextBlob in container.list_blobs(name_starts_with=path):
        if not hasTimeData:
            # We call parse time to add the time data for each blob
            parseTime(nextBlob, container, dateFreq, hourFreq)

        content = nextBlob.content_settings
        metadata = nextBlob.metadata

        # Azure returns a broad and specific dataType (e.g. img/jpeg), so we take
        # the broad datatype
        dataTypeBroad = content.content_type.split('/')[0]

        if(dataTypes.get(dataTypeBroad, None) != None):
            dataTypes[dataTypeBroad] += 1
        else:
            #When we haven't encountered this datatype yet
            dataTypes.setdefault(dataTypeBroad, 1)

        if(len(metadata.values()) > 0):
            #If there is actual metadata attached
            metadataCount += 1

        sizeCount += nextBlob.size
        blobsCount += 1

    # Gives us the relative file path ./summary
    filePath = os.path.dirname(__file__)
    filePath = os.path.join(filePath, './summary')

    if not os.path.exists(filePath):
        # We make the folder 'summary' if it does not yet exist
        os.mkdir(filePath)

    if(path == None):
        filePath = ''.join([filePath, f'/{container.container_name}_summary.txt'])
    else:
        if not os.path.exists(f"{filePath}/{container.container_name}"):
                # We make the subdirectory if it doesn't exist yet
                os.mkdir(f"{filePath}/{container.container_name}")

        fileName = path.split("/")
        fileName = '_'.join(fileName) + "summary.txt"
        filePath = ''.join([filePath, f'/{container.container_name}/{fileName}'])

    output = open(filePath, "w")

    if(path == None):
        print(f"Container: {container.container_name} has {blobsCount} blobs of which {metadataCount} have metadata")
        output.write(f"Container: {container.container_name} has {blobsCount} blobs of which {metadataCount} have metadata\n")
    else:
        print(f"Path: {container.container_name}/{path} has {blobsCount} blobs of which {metadataCount} have metadata")
        output.write(f"Path: {container.container_name}/{path} has {blobsCount} blobs of which {metadataCount} have metadata\n")

    print(f"Total Size (Bytes): {sizeCount}")
    output.write(f"Total Size (Bytes): {sizeCount}\n")

    if blobsCount != 0:
        # To avoid a division by zero error
        print(f"Average Size (Bytes): {sizeCount/blobsCount}")
        output.write(f"Average Size (Bytes): {sizeCount/blobsCount}\n")
    else:
        print(f"Average Size (Bytes): {sizeCount}")
        output.write(f"Average Size (Bytes): {sizeCount}\n")

    for nextDataType in dataTypes.items():
        print(f"There are {nextDataType[1]} blobs of type {nextDataType[0]}")
        output.write(f"There are {nextDataType[1]} blobs of type {nextDataType[0]}\n")

    print("")
    output.close()

    #we add some things to the dictionary and return it so we can use it in allBlobStats()
    dataTypes['sizeCount'] = sizeCount
    dataTypes['blobsCount'] = blobsCount
    dataTypes['metadataCount'] = metadataCount
    return dataTypes

# Function which provides a statistical summary on the overall blobs in the
# storage account
# param: client - The BlobServiceClient associated with the storage account
# param: containersList - The list of containers associated with the storage account
def allBlobStats(client, containersList):
    totalSize = 0
    totalBlobs = 0
    totalMetadata = 0
    dataTypes = {}

    for nextContainer in containersList:
        summary = containerStats(client.get_container_client(nextContainer['name']))
        for nextKey in summary.keys():
            # We add the values to their respective variables to update the total
            if(nextKey == "sizeCount"):
                totalSize += summary[nextKey]
            elif(nextKey == "blobsCount"):
                totalBlobs += summary[nextKey]
            elif(nextKey == "metadataCount"):
                totalMetadata += summary[nextKey]
            elif(dataTypes.get(nextKey, None) != None):
                dataTypes[nextKey] += summary[nextKey]
            else:
                dataTypes.setdefault(nextKey, summary[nextKey])

    path = os.path.dirname(__file__)
    path = os.path.join(path, './summary')

    if not os.path.exists(filePath):
        # We make the folder 'summary' if it does not yet exist
        os.mkdir(filePath)

    path = ''.join([path, f'/allBlobs_summary.txt'])

    output = open(path, "w")

    print(f"Total Blobs: {totalBlobs}")
    print(f"Total Blobs with Metadata: {totalMetadata}")
    print(f"Total Size (Bytes): {totalSize}")
    output.write(f"Total Blobs: {totalBlobs}\n")
    output.write(f"Total Blobs with Metadata: {totalMetadata}\n")
    output.write(f"Total Size (Bytes): {totalSize}\n")

    if totalBlobs != 0:
        print(f"Average Size (Bytes): {totalSize/totalBlobs}")
        output.write(f"Average Size (Bytes): {totalSize/totalBlobs}\n")
    else:
        # To avoid a division by zero error
        print(f"Average Size (Bytes): {totalSize}")
        output.write(f"Average Size (Bytes): {totalSize}\n")

    for nextDataType in dataTypes.items():
        print(f"There are {nextDataType[1]} blobs of type {nextDataType[0]}")
        output.write(f"There are {nextDataType[1]} blobs of type {nextDataType[0]}\n")

    output.close()

# Function which writes a csv to data representing the frequenecy of dates
# and hours (syd time)
# param: container - The container client associated with the container we're interested in
# param: dateFreq - The 2 dimensional dictionary associated with Date frequency data
# param: hourFreq - The 2 dimensional dictionary associated with Hour frequency data
def timeFreq(container, dateFreq, hourFreq, path=None):
    if(dateFreq.get(container.container_name) == None and path == None):
        # When we don't have date data for a container yet
        dateFreq.setdefault(container.container_name, {})
        hourFreq.setdefault(container.container_name, {})

        for blob in container.list_blobs():
            parseTime(blob, container, dateFreq, hourFreq)

    elif(path != None):
        # We treat paths as a separate entity to dateFreq and hourFreq
        pathDateFreq = {}
        pathHourFreq = {}
        pathDateFreq.setdefault(container.container_name, {})
        pathHourFreq.setdefault(container.container_name, {})

        for blob in container.list_blobs(name_starts_with=path):
            parseTime(blob, container, pathDateFreq, pathHourFreq)

    # Conversion to csv file
    if path == None:
        dates = pd.DataFrame.from_dict(dateFreq[container.container_name], orient="index", columns=["count"])
        hours = pd.DataFrame.from_dict(hourFreq[container.container_name], orient="index", columns=["count"])
    else:
        dates = pd.DataFrame.from_dict(pathDateFreq[container.container_name], orient="index", columns=["count"])
        hours = pd.DataFrame.from_dict(pathHourFreq[container.container_name], orient="index", columns=["count"])

    dates.index.name="date"
    hours.index.name="hour"

    # The path for the file
    filePath = os.path.dirname(__file__)
    filePath = os.path.join(filePath, './data')

    if not os.path.exists(filePath):
        # We make the folder 'data' if it does not yet exist
        os.mkdir(filePath)

    if path == None:
        hoursPath = ''.join([filePath, f'/{container.container_name}_hours.csv'])
        datesPath = ''.join([filePath, f'/{container.container_name}_dates.csv'])
    else:
        if not os.path.exists(f"{filePath}/{container.container_name}"):
            # We make the subdirectory if it does not exist
            os.mkdir(f"{filePath}/{container.container_name}")

        fileName = path.split("/")
        fileName = '_'.join(fileName)
        hoursPath = ''.join([filePath, f'/{container.container_name}/{fileName}hours.csv'])
        datesPath = ''.join([filePath, f'/{container.container_name}/{fileName}dates.csv'])

    hours.to_csv(hoursPath)
    dates.to_csv(datesPath)
    print(f"Frequency of hours and dates for {container.container_name} written to csv")

# Function which writes to a csv all the time frequency data for all containers
# param: client - The BlobServiceClient associated with the storage account
# param: containersList - The list of containers associated with the storage account
# param: dateFreq - The 2 dimensional dictionary associated with Date frequency data
# param: hourFreq - The 2 dimensional dictionary associated with Hour frequency data
def allTimeFreq(client, containerslist, dateFreq, hourFreq):
    for nextContainer in containersList:
        currentContainer = client.get_container_client(nextContainer)
        timeFreq(currentContainer, dateFreq, hourFreq)

# Function which is responsible for parsing a blob and adding its time data to
# a dictionary or creating an entry of dictionary if not encountered yet
# param: blob - The blob which we are parsing
# param: containersList - The list of containers associated with the storage account
# param: dateFreq - The 2 dimensional dictionary associated with Date frequency data
# param: hourFreq - The 2 dimensional dictionary associated with Hour frequency data
def parseTime(blob, container, dateFreq, hourFreq):
    # Conversion to Australian/Sydney time
    blobDate = blob.creation_time.astimezone(timezone('Australia/Sydney')).date()
    blobTime = blob.creation_time.astimezone(timezone('Australia/Sydney')).hour

    if(dateFreq[container.container_name].get(blobDate, None) != None):
        dateFreq[container.container_name][blobDate] += 1
    else:
        dateFreq[container.container_name].setdefault(blobDate, 1)


    if(hourFreq[container.container_name].get(blobTime, None) != None):
        hourFreq[container.container_name][blobTime] += 1
    else:
        hourFreq[container.container_name].setdefault(blobTime, 1)

try:
    env = Env()
    env.read_env()

    accessKey = env("ACCESS_KEY")
    accountName = env("ACCOUNT_NAME")

    if(accessKey and accountName != ""):
        # Initially try connecting via accesskey and accountname
        client = BlobServiceClient(f"https://{accountName}.blob.core.windows.net", accessKey)
    else:
        raise NameError
except Exception as e:
    try:
        env = Env()
        env.read_env()

        connect_str = env("CONNECT_STR")

        if(connect_str != ""):
            # Or else try connectstr if accesskey and accountname doesn't work
            client = BlobServiceClient.from_connection_string(connect_str)
        else:
            raise NameError
    except Exception as e:
        print("Ensure that .env variables have been set and that the .env file exists in the directory!")
        print(e)
        sys.exit()

containersList = list(client.list_containers())
commands = {
    # Matches command line inputs to an appropriate function
    "quit": quit,
    "help": help,
    "containers": partial(listContainers, containersList),
    "allblobs": partial(listAllBlobs, client, containersList, dateFreq=dateFreq, hourFreq=hourFreq),
    "alldetails": partial(allBlobStats, client, containersList),
    "blobs": partial(listContainerBlobs, dateFreq=dateFreq, hourFreq=hourFreq),
    "timefreq": partial(timeFreq, dateFreq=dateFreq, hourFreq=hourFreq),
    "details": partial(containerStats, path=None),
    "alltimefreq": partial(allTimeFreq, client, containersList, dateFreq, hourFreq)
}

# Set of names of containers for faster lookup
containerNames = set()
for next in containersList:
    containerNames.add(next['name'])

print("Welcome to the Blob Storage data exploration script!\n")
print("Note that depending on the amount of blobs, this script can be quite slow")
print("Type help for a list of commands")

while True:
    nextCommand = input("\n").lower()
    print("")

    if(commands.get(nextCommand, False) != False and nextCommand != "blobs" and nextCommand != "timefreq"):
        #if the input matches one of our commands in the dictionary
        commands[nextCommand]()
    elif (len(nextCommand.split(" ")) == 2):
        # if input matches one of the commands in the dictionary, for two argument commands
        blobPath = nextCommand.split(" ")[1].split("/") #used for the path of blobs

        if(commands.get(nextCommand.split(" ")[0], False) != False and blobPath[0] in containerNames):
            if(len(blobPath) > 1):
                # When we are also passing a path to explore a certain subdirectory of a container
                # e.g. site-images/user_uploads/<user id>

                path = ""
                for i in range (1, len(blobPath)):
                    path += blobPath[i] + "/"

                commands[nextCommand.split(" ")[0]](client.get_container_client(blobPath[0]), path=path)
            else:
                commands[nextCommand.split(" ")[0]](client.get_container_client(nextCommand.split(" ")[1]))
        else:
            print("Invalid Command")
    else:
        print("Invalid Command")
