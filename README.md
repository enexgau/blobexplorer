# Introduction

This tool is used to gain valuable data insights into the data stored within blob storage accounts.
Commands are given using the help function or are listed below in the usage section. Depending on
the amount of blobs and size of each blob, this script can take anywhere from a few seconds to minutes.
(Ballpark around 1000 blobs/sec! so quite slow).

Time Frequency data is written to the /data folder with time frequency data on folders within a container are
stored within /data/containername. Similarly for summary data, this is stored in the /summary folder, with
summary data on folders within a container being stored within /summary/containername.

**For site navigator project group navigation please read projectGroups.md**

# Installation
`pip install environs`

`pip install azure-storage blob`

Use --user flag if installation not allowed

# Setup

Ensure there is a file called .env in the same directory as main.py. If there is not one, create one with
`touch .env`

and create the variables ACCCESS_KEY, ACCOUNT_NAME and CONNECT_STR

Add the ACCESS_KEY and ACCOUNT_NAME to the .env files for your Azure blob storage account.
Alternatively, add the connection string for your account instead in the .env file
The access key can be found under settings > access keys on storage explorer along with the account name.
The connection string can also be found under settings > access keys for your storage account

# Usage

`python3 main.py`

# Commands
`containers` - Lists all containers for the storage account

`allblobs` - Lists ALL the blobs in the storage account (Lengthy process)

`alldetails` - Lists details for ALL the containers in the storage account and writes to file (Length process)

`alltimefreq` - Writes the frequency data of dates and the hour of upload for blobs to a csv file. (Note running alldetails first will make this command much faster)

`blobs <name>` - Lists the blobs for a destination given by <name>. Name is a specified container OR a specified container file path. This allows for navigation of folders underneath a container to get data only on the content under that folder e.g. container site-images with subfolder user_uploads can be explored using `blobs site-images/user_uploads`

`timefreq <name>` - Lists the date frequency and hour frequency for a destination given by <name>

`details <name>` - Lists the details for a destination given by <name>

`help` - Prints the command guide

`quit` - Exits the program
